# tinygeom
minimal geometry processing

- vector class
- addition, substraction
- external (scalar) multiplication
- dot product
- cross product
- distance
- norm
- angle of three ordered points
- gradient of angle projected in a plane
