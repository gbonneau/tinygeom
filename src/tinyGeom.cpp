#include <iostream>
#include <assert.h>
#include "tinyGeom.h"

#define _TEST_
#ifdef _TEST_
void testAddition ()
{
    tinyGeom::vector v1(1.,0.,1.), v2(0.,1.,0.);
    tinyGeom::vector v3 = v1+v2;
    assert( v3 == tinyGeom::vector(1.,1.,1.) );
}

void testSubstraction()
{
    tinyGeom::vector v1(1.,0.,1.), v2(0.,1.,0.);
    tinyGeom::vector v3 = v1-v2;
    assert( v3 == tinyGeom::vector(1.,-1.,1.) );
}

void  testExternalProduct()
{
    tinyGeom::vector v1(1.,0.,1.), v2(0.,1.,1.);
    double result = v1.dot(v2);
    assert( result == 1.);
}

void testCrossProduct()
{
    tinyGeom::vector v1(1.,0.,1.), v2(0.,1.,1.);
    tinyGeom::vector v = v1.cross(v2);
    assert( v == tinyGeom::vector( -1., -1., 1.) );
}

void testAngleFromThreeOrderedPoints()
{
    double EPSILON = 1e-15;
    tinyGeom::vector vAvant(3.,0.,0.), vCentre(0.,0.,0.), vApres(0., 2., 0.);
    double result = vAvant.angle( vCentre, vApres);
    printf("result = %20.18lf PI/2 = %20.18lf\n", result, M_PI/2.);
    assert( fabs(result-M_PI/2.) < EPSILON);

    vAvant = tinyGeom::vector( 3.,0.,0.);
    vCentre = tinyGeom::vector( 0.,0.,0.);
    vApres = tinyGeom::vector( 2.,2.,0.);
    // result = tinyGeom::angle( vAvant, vCentre, vApres);
    result = vAvant.angle( vCentre, vApres);
    printf("result = %20.18lf PI/4 = %20.18lf\n", result, M_PI/4.);
    assert( fabs(result-M_PI/4.) < EPSILON);
}

void testDistanceToPoint()
{
    tinyGeom::vector v1(1.,0.,1.), v2(0.,1.,0.);
    assert( v1.distance(v2) == sqrt(3.));
}

void testGradientTheta()
{
    tinyGeom::vector v1(1.,0.,0.), V(0.,0.,1.);

    std::cout << "gradient theta for vectors " << v1 << " and " << V << ": " << v1.gradientTheta(V) << std::endl;
}

int main()
{
    testAddition();
    testSubstraction();
    testExternalProduct();
    testCrossProduct();
    testAngleFromThreeOrderedPoints();
    testDistanceToPoint();
    testGradientTheta();

    std::cout << "all tests passed\n";
}
#endif