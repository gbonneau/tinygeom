/**
 * @file tinyGeom.h
 * @author Georges-Pierre Bonneau (Georges-Pierre.Bonneau@inria.fr)
 * @brief 
 * @version 0.1
 * @date 2020-03-19
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef _TINY_GEOM_
#define _TINY_GEOM_

#include <iostream>
#include <math.h>
#include <cstdint>
#include <algorithm>
#include <iostream>
#include <iomanip>

namespace tinyGeom{

        class vector{

        private:
            /**
             * @brief cartesian coordinates of the vector
             * 
             */
            double cartCoord[3];

        public:
            /**
             * @brief Construct a new vector object
             * 
             */
            vector() {
                cartCoord[0] = 0.;
                cartCoord[1] = 0.;
                cartCoord[2] = 0.;
            }
            /**
             * @brief Construct a new vector object from double coordinates
             * 
             * @param[in] x x coordinate
             * @param[in] y y coordinate
             * @param[in] z z coordinate
             */
            vector( const double x, const double y, const double z) {
                cartCoord[0] = x;
                cartCoord[1] = y;
                cartCoord[2] = z;
            }
            /**
             * @brief Construct a new vector object
             * 
             * @param c array of 3 x, y and z coordinates
             */
            vector( const double *c) {
                cartCoord[0] = c[0];
                cartCoord[1] = c[1];
                cartCoord[2] = c[2];
            }
            /**
             * @brief overload of the ostream operator << for vector
             * 
             * @param[out] os 
             * @param[in] v 
             * @return std::ostream& 
             */
            friend std::ostream& operator << (std::ostream& os, const vector& v)
            {
                os << std::fixed << v.cartCoord[0] << " " <<  v.cartCoord[1] << " " << v.cartCoord[2];
                return os;
            }

            /**
             * @brief set x coordinate
             * 
             * @param a 
             */
            void x( const double a)
            {
                this->cartCoord[0] = a;
            }

            /**
             * @brief return x coordinate
             * 
             * @return double 
             */
            double x()
            {
                return this->cartCoord[0];
            }

            /**
             * @brief set y coordinate
             * 
             * @param a 
             */
            void y( const double a)
            {
                this->cartCoord[1] = a;
            }

            /**
             * @brief return y coordinate
             * 
             * @return double 
             */
            double y()
            {
                return this->cartCoord[1];
            }

            /**
             * @brief set z coordinate
             * 
             * @param a 
             */
            void z( const double a)
            {
                this->cartCoord[2] = a;
            }
            
            /**
             * @brief return z coordinate
             * 
             * @return double 
             */
            double z()
            {
                return this->cartCoord[2];
            }

            /**
             * @brief return the coordinate #direction
             * 
             * @param direction 
             * @return double 
             */
            double c(int direction) {
                return this->cartCoord[ direction];
            }

            /**
             * @brief copy the coordinates in an array
             * 
             * @param c 
             */
            void getCoordinates( double *c) {
                c[0] = this->cartCoord[0];
                c[1] = this->cartCoord[1];
                c[2] = this->cartCoord[2];
            }
            
            /**
             * @brief overloading of the addition operator for two vectors
             * 
             * @param[in] v2 vector to be added 
             * @return vector 
             */
            vector operator +(const vector v2) {
                vector result;
                result.cartCoord[0] = this->cartCoord[0] + v2.cartCoord[0];
                result.cartCoord[1] = this->cartCoord[1] + v2.cartCoord[1];
                result.cartCoord[2] = this->cartCoord[2] + v2.cartCoord[2];
                return result;
            }
            /**
             * @brief overloading of the substraction operator for two vectors
             * 
             * @param v2 
             * @return vector 
             */
            vector operator -(const vector v2) {
                vector result;
                result.cartCoord[0] = this->cartCoord[0] - v2.cartCoord[0];
                result.cartCoord[1] = this->cartCoord[1] - v2.cartCoord[1];
                result.cartCoord[2] = this->cartCoord[2] - v2.cartCoord[2];
                return result;
            }
            /**
             * @brief compare two vectors. TODO: approximate comparison
             * 
             * @param v1 
             * @param v2 
             * @return true 
             * @return false 
             */
            friend bool operator ==(const vector v1, const vector v2) {
                bool result=true;
                result = result && (v1.cartCoord[0] == v2.cartCoord[0]);
                result = result && (v1.cartCoord[1] == v2.cartCoord[1]);
                result = result && (v1.cartCoord[2] == v2.cartCoord[2]);
                return result;
            }
            /**
             * @brief product of a scalar and a vector
             * 
             * @param a 
             * @param v 
             * @return vector 
             */

            friend vector operator *(const double a, const vector v) {
                vector result;
                result.cartCoord[0] = v.cartCoord[0] * a;
                result.cartCoord[1] = v.cartCoord[1] * a;
                result.cartCoord[2] = v.cartCoord[2] * a;
                return result;
            }

            /**
             * @brief scalar-product of two vectors
             * 
             * @param v1 
             * @param v2 
             * @return double 
             */
            double dot( vector v2) {
                double result;
                result = this->cartCoord[0] * v2.cartCoord[0];
                result += this->cartCoord[1] * v2.cartCoord[1];
                result += this->cartCoord[2] * v2.cartCoord[2];
                return result;
            }
            /**
             * @brief cross product of two vectors
             * 
             * @param v2 
             * @return tinyGeom::vector 
             */
            tinyGeom::vector cross( vector v2) {
                double x, y, z;
                x = this->cartCoord[1] * v2.cartCoord[2] - this->cartCoord[2] * v2.cartCoord[1];
                y = -this->cartCoord[0] * v2.cartCoord[2] + this->cartCoord[2] * v2.cartCoord[0];
                z = this->cartCoord[0] * v2.cartCoord[1] - this->cartCoord[1] * v2.cartCoord[0];
                return tinyGeom::vector( x, y, z);
            }

            /**
             * @brief determinant of three vectors
             * 
             * @param v2 
             * @param v3 
             * @return double 
             */
            double det( vector v2, vector v3) {
                return v3.dot( this->cross(v2));
            }
            /**
             * @brief norm of vector
             * 
             * @return double 
             */
            double norm()
            {
                return( sqrt( (*this).dot(*this)));
            }
            /**
             * @brief distance with to a point
             * 
             * @param v2 
             * @return double 
             */
            double distance( vector v2) {
                vector v = v2 - (*this);
                return( v.norm());
            }

            /**
             * @brief return the angle formed with two other points
             * 
             * @param PCentre 
             * @param PApres 
             * @return double 
             */
            double angle( vector P1, vector P2) {
                vector V1 = *this - P1;
                vector V2 = P2 - P1;
                V1 = (1./V1.norm()) * V1;
                V2 = (1./V2.norm()) * V2;
                double Angle = acos( V1.dot(V2));
                // printf("%20.15lf %20.15lf\n", V1.dot(V2), Angle);
                return Angle;
            }

            /**
             * @brief gradient of a signed angle (theta) when the tip of the vector vary infinitesimaly,
             * projected in the plane orthogonal to V where U = this.
             * The angle is given by determinant( U, U+dU, V)
             * 
             * @param V vector that determines the plane in which the angle is measured
             * @return tinyGeom::vector 
             */
            tinyGeom::vector gradientTheta( vector V)
            {
                tinyGeom::vector gradient;
                double EPSILON = 1e-8;
                double nSquare = this->dot(*this);

                tinyGeom::vector e1(1.,0.,0.);
                tinyGeom::vector e2(0.,1.,0.);
                tinyGeom::vector e3(0.,0.,1.);


                if (V.norm() > EPSILON) {
                // if (1) {
                    gradient = tinyGeom::vector( V.dot(this->cross(e1)), V.dot(this->cross(e2)), V.dot(this->cross(e3)));
                    gradient = (1./(nSquare*V.norm())) * gradient;
                    // printf("gradient.x = %lf\n", gradient.x());
                    // printf("gradient.y = %lf\n", gradient.y());
                    // printf("gradient.z = %lf\n", gradient.z());
                    // gradient.x( acos(std::min( 1., std::max( -1., gradient.x()))));
                    // gradient.y( acos(std::min( 1., std::max( -1., gradient.y()))));
                    // gradient.z( acos(std::min( 1., std::max( -1., gradient.z()))));
                } else {
                    printf("=== Probleme numerique potentiel!!! === \n");
                    // exit(-1);
                    gradient = tinyGeom::vector((this->cross(e1)).norm(), (this->cross(e2)).norm(), (this->cross(e3)).norm());
                    gradient = (1./this->norm()) * gradient;

                }
                return gradient;
            }

    }; // class vector
}


#endif // _TINY_GEOM_